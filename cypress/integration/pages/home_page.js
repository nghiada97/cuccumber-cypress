import  BasePage  from "./base_page";
import locator from "../locator/home_page_lc";

class HomePage extends BasePage {
    constructor(){
        super();
    }

    click_login_button(){
        this.click_element(locator.login_input)
    }

    send_key_email_and_password_field(email, pw){
        this.send_key_locator(locator.email_field, email)
        this.send_key_locator(locator.pw_field, pw)
    }

    click_access_login_button(){
        this.click_element(locator.access_login_button)
    }

    verify_error_mess(){
        cy.xpath(locator.error_mess).should("have.text", "Email đăng nhập hoặc mật khẩu không chính xác, vui lòng kiểm tra lại.")
    }
}

export default HomePage;