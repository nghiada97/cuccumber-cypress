/// <reference types="Cypress" />
require('cypress-xpath')

Cypress.on("uncaught:exception", (err, runnable) => {
    return false;
});

class BasePage {


    click_element(element_locator) {
        cy.xpath(element_locator, {timeout: 60000}).click({ multiple: true, force: true });
    }

    click_element_by_string(string) {
        cy.contains(string, {timeout: 60000}).click()
    }

    click_first_element(element_locator) {
        cy.xpath(element_locator).first().click({ multiple: true, force: true });
    }

    send_key_locator(element_locator, key) {
        cy.xpath(element_locator, { timeout: 60000 }).clear({ force: true }).type(key, { force: true });
    }

    upload_file(element_locator, file_to_upload) {
        cy.xpath(element_locator).attachFile(file_to_upload, {
            subjectType: "drag-n-drop",
        });
    }

    send_key_locator_in_iframe(iframe, locator, key) {
        // fill in a value in iframe
        cy.xpath(iframe).then($iframe => {
            const iframe = $iframe.contents();
            const myInput = iframe.find(locator)
            cy.wrap(myInput).type(key, { force: true });
        });
    }

    verify_text_visible_by_element(element_locator, text){
        cy.xpath(element_locator, {timeout: 60000}).then(($el)=>{
            expect($el.text()).to.include(text)
        })
    }

    verify_include_url(url) {
        expect(cy.url().should("include", url));
    }

    verify_not_include_url(url) {
        expect(cy.url().should("not.include", url));
    }

    verify_element_visible_by_string(string) {
        expect(cy.contains(string, { timeout: 60000 }).should("be.visible"));
    }

    verify_element_visible_by_locator(locator) {
        expect(cy.xpath(locator, { timeout: 60000 }).should("be.visible"));
    }

    verify_element_not_visible_by_string(string) {
        expect(cy.contains(string, { timeout: 60000 }).should("not.be.visible"));
    }

    verify_element_have_lenght(element_locator, length) {
        cy.xpath(element_locator).should(($p) => {
            expect($p).to.have.length(length)
        })
    }

    verify_element_visible_by_input(locator, string) {
        cy.xpath(locator).should('have.value', string)
    }

    verify_element_disable_by_locator(locator) {
        expect(cy.xpath(locator, { timeout: 60000 }).should("be.disabled"));
    }

    string_random(text, number) {
        let r = Math.random().toString(36).substring(number + 2);
        let txt = text + r;
        return txt;
    }

    index_random(index) {
        let r = Math.random().toString(9).substring(2, 2 + index);
        return r;
    }

    email_random(email_name) {
        let r = Math.random().toString(36).substring(9);
        let txt = email_name + "+" + r + "@gmail.com"
        return txt
    }

    get_expiry_date() {
        // Get expiry date +1 month with form (month)/(2 last number in year)
        var date = new Date();
        let prevExpiryDate = (date.getMonth() + 1) + "/" + (date.getFullYear() + 1 ).toString().substring(2,5)
        return prevExpiryDate
    }

    count_lenght_locator(locator) {
        cy.xpath(locator).then(($el) => {
            return ($el.find(locator).length)
        })
    }

    get_dateTime_now() {
        let day = new Date();
        let date = day.getDay() + ':' + day.getMonth() + ':' + day.getFullYear();
        let hour = day.getHours() + ':' + day.getMinutes() + ':' + day.getSeconds();
        return date + ' ' + hour
    }

    verify_string_not_exits(string) {
        cy.contains(string).should('not.exist')
    }

}

export default BasePage;