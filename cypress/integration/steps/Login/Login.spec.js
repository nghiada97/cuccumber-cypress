const {Before,After,Given,Then, When, And} = require("cypress-cucumber-preprocessor/steps");
import HomePage  from "../../pages/home_page";
let home_page = new HomePage()

Before(() => {      //belongs to the cucumber
    cy.visit("https://www.hotdeal.vn/ha-noi/");
    });

Before({ tags: "@SmokeTest" },() => {      //belongs to the cucumber
    cy.log("Smoke Test")
  });

Given("I click on login button", () => {
    home_page.click_login_button()
});

When("I fill in email field with value {string} and password field with value {string}",(email, pw) =>{
    home_page.send_key_email_and_password_field(email, pw)
})

And("I click on access login button", ()=>{
    home_page.click_access_login_button()
})

Then("I check displayed error mess", ()=>{
    home_page.verify_error_mess()
})

When('I fill in email field and password field with data table {string} and {string}',(email,password ) =>{
        home_page.send_key_email_and_password_field(email, password)
})
