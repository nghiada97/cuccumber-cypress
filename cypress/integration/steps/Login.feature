Feature: Login

    I want login

    Background: Something
        Given I click on login button


    Scenario: Login
        When I fill in email field with value "nghida97@gmail.com" and password field with value "Nghiadzvl"
        And I click on access login button
        Then I check displayed error mess

    @SmokeTest 
    Scenario Outline: Login with table
        When I fill in email field and password field with data table '<email>' and '<password>'
        And I click on access login button
        Then I check displayed error mess
        Examples:
            | email                | password |
            | Nghia97+1@gmail.com  | Nghiadzvl  |
            | Nghia97+2@gmail.com  | Nghiadzvl  |